import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    contacts: JSON.parse(localStorage.getItem('contacts')) || []
  },
  mutations: {

  },
  actions: {

  },
  getters: {
  },
  modules: {
  }
})
