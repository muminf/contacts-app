import Vue from 'vue'
import Vuex from 'vuex'
import contact from './contact'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
    addContact({commit},contact){
      this.state.contact.contacts.push(contact)
      localStorage.setItem('contacts', JSON.stringify(this.state.contact.contacts))
    },
    updateContact({commit},contact){
      this.state.contact.contacts = this.state.contact.contacts.map(el => el.id == contact.id ? contact : el)
      localStorage.setItem('contacts', JSON.stringify(this.state.contact.contacts))
    },

    removeContact({commit},id){
      this.state.contact.contacts = this.state.contact.contacts.filter(el => el.id !== id)
      localStorage.setItem('contacts', JSON.stringify(this.state.contact.contacts))
    }
  },
  actions: {
    getById({commit, dispatch},id){
      return this.state.contact.contacts.find(el => el.id == id)
    }
  },
  getters: {

  },
  modules: {
    contact
  }
})
